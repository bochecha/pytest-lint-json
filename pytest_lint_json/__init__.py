# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-json
#
# pytest-lint-json is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-json is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-json.  If not, see <http://www.gnu.org/licenses/>.


import json

from jsonschema import (
    validate as validate_json,
    ValidationError as JSONValidationError,
)

import py
import pytest

from .schemas import SchemaRegistry


class JSONCollector(pytest.collect.File):
    def collect(self):
        return (
            JSONItem(self.fspath, parent=self),
        )


class JSONItem(pytest.collect.Item):
    def __init__(self, fspath, parent=None):
        super().__init__(fspath.relto(py.path.local()), parent=parent)
        self.fspath = fspath
        self.add_marker('lint_json')

    def runtest(self):
        data = json.loads(self.fspath.read())
        schema = self.config._jsonschemas.get(self.fspath.ext)

        if schema:
            validate_json(data, schema)

    def repr_failure(self, excinfo):
        if excinfo.errisinstance(json.decoder.JSONDecodeError):
            return excinfo.value.args[0]

        if excinfo.errisinstance(JSONValidationError):
            path = '/%s' % '/'.join(str(item) for item in excinfo.value.path)
            return 'At %s: %s' % (path, excinfo.value.args[0])

        return super().repr_failure(excinfo)

    def reportinfo(self):
        return (self.name, -1, 'JSON %s' % self.name)


def pytest_addoption(parser):
    group = parser.getgroup('general')
    group.addoption(
        '--lint-json', action='store_true', help='Verify your JSON files')
    parser.addini(
        'jsonextensions', type='linelist', default=['.json'],
        help='the comma-separated list of file extensions to treat as JSON '
             'files. (defaults to ".json")')
    parser.addini(
        'jsonschemas', type='linelist', default=[],
        help='the comma-separated list of schemas to use for each extension. '
             '(defaults to not using any schema)')


def pytest_sessionstart(session):
    config = session.config

    if config.option.lint_json:
        config._jsonextensions = config.getini('jsonextensions')

        schema_specs = config.getini('jsonschemas')
        schemas = SchemaRegistry()

        for spec in schema_specs:
            extension, name = spec.split('=')
            schemas.add_schema(extension, name)

        config._jsonschemas = schemas


def pytest_collect_file(path, parent):
    config = parent.config

    if not config.option.lint_json:
        return

    if path.ext in config._jsonextensions:
        return JSONCollector(path, parent=parent)
