# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-json
#
# pytest-lint-json is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-json is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-json.  If not, see <http://www.gnu.org/licenses/>.


import json
from pathlib import Path


class SchemaRegistry(dict):
    def __getitem__(self, key):
        try:
            return super().__getitem__(key)

        except KeyError:
            raise UnknownSchemaError(key)

    def __setitem__(self, *_):
        raise NotImplementedError('Please use `add_schema` instead')

    def add_schema(self, key, name):
        if name.startswith('file:'):
            # Custom schema
            path = Path(name[5:])

        else:
            # One of the known schemas
            path = Path(__file__).parent / ('%s.schema' % name)

        super().__setitem__(key, json.load(path.open(mode='r')))


class UnknownSchemaError(Exception):
    pass
