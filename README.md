# pytest-lint-json

This [Pytest](https://pytest.org) plugin allows you to ensure your JSON files
are all valid.

# Usage

Install the plugin:

    $ pip install pytest-lint-json

Then tell Pytest to use it:

    $ py.test --lint-json

# Configuration

## File Extensions

By default pytest-lint-json will find all files with the `.json` extension and
check them.

You can specify a list of extensions to treat as JSON files with the
`jsonextensions` option, for example in your `setup.cfg` file:

```ini
[tool:pytest]
jsonextensions =
    .foo
    .bar
```

## JSON Schemas

You might want to additionally validate your JSON files against a
[JSON schema](http://json-schema.org/).

To do this, simply specify the schema to use for each JSON extension with the
`jsonschemas` option:

```ini
[tool:pytest]
jsonschemas =
    .foo=file:foo.schema
    .bar=file:path/to/bar.schema
```

Each line associates a schema to an extension.

Using `file:` tells pytest-lint-json to use a custom schema. The part after the `:`
is the relative path to a file containing the schema.

pytest-lint-json also comes with a set of builtin schemas you can use by simply
specifying their name, without the `file:` part. At the moment, those are:

* `schema`, [a JSON schema to validate JSON schemas](http://json-schema.org/schema)

For example, if you want to validate the `.foo` files with your custom schema,
and also validate that schema, you could use:

```ini
[tool:pytest]
jsonextensions =
    .foo
    .schema
jsonschemas =
    .foo=file:foo.schema
    .schema=schema
```

# Legalities

pytest-lint-json is offered under the terms of the
[GNU Affero General Public License, either version 3 or any later version](http://www.gnu.org/licenses/agpl.html).

We will never ask you to sign a copyright assignment or any other kind of
silly and tedious legal document before accepting your contributions.

In case you're wondering, we do **not** consider that using pytest-lint-json makes
your JSON files licensed under the AGPL.
